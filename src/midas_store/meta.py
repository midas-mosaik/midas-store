META = {
    "type": "time-based",
    "models": {
        "Database": {
            "public": True,
            "any_inputs": True,
            "params": ["filename", "verbose", "buffer_size", "keep_old_files"],
            "attrs": [],
        }
    },
}
